import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
public class databazeFinal {
    public static ArrayList<String> list=new ArrayList<String>();
    public static Scanner sc=new Scanner(System.in);
    public static String vstup=null;
    public static void main(String args[]) {
        //Scanner sc=new Scanner(System.in);
        String input=new String();
        while (true){
            System.out.println("Zvolte, co chcete delat: \n1 - pridat osobu \n2 - odebrat osobu \n3 - vyhledat osobu \nNebo napiste 'Konec'");
            input=sc.next();
            vstup=null;
            if(input.equals("Konec")) break;
            else try{
                if(Integer.parseInt(input)==1){
                    ziskejUdaje();  
                }
                else if(Integer.parseInt(input)==2){
                    if(list.size()==0){ 
                        System.out.println("Databaze je prazdna");
                        continue;
                    }
                    else odstranOsobu();
                }
                else if(Integer.parseInt(input)==3){
                    if(list.size()==0){
                        System.out.println("Databaze je prazdna");
                        continue;
                    }
                    else hledej();
                }
                }  
                    catch(Exception e){
                        System.out.println("Pro vyber akce napiste cislo (1-3) nebo 'Konec' pro ukonceni");
                }
        }
    }
    public static void ziskejUdaje(){
        String pr=null;
        String lom=null;
        Boolean exituje=false;
        String vstup;
        System.out.println("Zadejte jméno: ");
        String jm=null;
        jm=sc.next();
        while(jm=="") {
            System.out.println("Zadejte platne jmeno: ");
            jm=sc.next();
        }
        list.add(jm);
        System.out.println("Zadejte prijmeni: ");
        pr=sc.next();
        while(pr==""){
            System.out.println("Zadejte platne prijmeni: ");
            pr=sc.next();
        }
        list.add(pr);
        while (true){
            try {
                lom=null;
                exituje=false;
                vstup=sc.nextLine();                            
                if(vstup.equals("Zrusit")) {
                    list.remove(list.size()-1);
                    list.remove(list.size()-1);
                    break;
                }
                else if(Integer.parseInt(vstup.substring(2, 4))>12 && Integer.parseInt(vstup.substring(2, 4))<51 || Integer.parseInt(vstup.substring(2, 4))>62){
                    System.out.println("Mesic v rodnem cisle je vyssi nez 12 nebo v pripade zeny neni v rozmezi 51 - 62. Zadejte spravne rodne cislo: ");
                    continue;
                }
                else if(Integer.parseInt(vstup.substring(4, 6))>31){
                    System.out.println("Den v rodnem cisle je vyssi nez 31, zadejte spravne rodne cislo: ");
                    continue;
                }
                Integer.parseInt(vstup.substring(0,2));
                lom=vstup.substring(6, vstup.length()).replaceAll("/", "");
                if(lom.length()==4) {
                    Integer.parseInt(lom);
                    for(int i=2;i<list.size();i+=3){
                        if(list.get(i).equals(vstup.replaceAll("/", ""))){
                            System.out.println("Zadene rodne cislo jiz existuje.\nZadejte spravne rodne cislo nebo napiste 'Zrusit': ");
                            exituje=true;
                        }
                    }
                    if(exituje) continue;
                    list.add(vstup.replaceAll("/", ""));
                    System.out.println("Osoba pridana.");
                    break;
                }
                else System.out.println("Zadejte spravne rodne cislo nebo napiste 'Zrusit': ");           
            } catch (Exception e) {
                System.out.println("Zadejte spravne rodne cislo nebo napiste 'Zrusit': ");
            }
        }
    }
    public static void odstranOsobu(){
        String vstup=null;
        System.out.println("Zadejte rodne cislo osoby, kterou chcete odebrat: ");
        vstup=sc.next();
        try{
            for(int i=2;i<list.size();i+=3){
                if(vstup.equals(list.get(i)) || vstup.replaceAll("/", "").equals(list.get(i))){
                    list.remove(i-2);
                    list.remove(i-2);
                    list.remove(i-2);
                    System.out.println("Osoba odstranena.");
                    break;
                }
                else if(i==list.size()-1) System.out.println("Zadana osoba nenalezena.");
            }
        }
        catch (Exception e){
            System.out.println("Zadana osoba nenalezena.");
        }
    }
    
    public static void hledej(){
        System.out.println("Zadejte rodne cislo osoby, kterou chcete vyhledat: ");
        vstup=null;
        vstup=sc.next();
        Date theDate;
        try{
            for(int i=2;i<list.size();i+=3){
                if(vstup.equals(list.get(i)) || vstup.replaceAll("/", "").equals(list.get(i))){
                    System.out.println("Osobni udaje osoby:\nJmeno: "+list.get(i-2)+"\nPrijmeni: "+list.get(i-1)+"\nRodne cislo: "+list.get(i).substring(0,6)+"/"+list.get(i).substring(6));                                
                    if(Integer.parseInt(list.get(i).substring(2,4))<=12){
                        theDate=new SimpleDateFormat("yyMMdd").parse(list.get(i).substring(0, 6));
                    }
                    else{
                        int mes=Integer.parseInt(list.get(i).substring(2, 4))-50;
                        String dat=null;
                        if(mes>10) dat=list.get(i).substring(0, 2)+mes+list.get(i).substring(4, 6);
                        else dat=list.get(i).substring(0, 2)+"0"+mes+list.get(i).substring(4, 6);
                        theDate=new SimpleDateFormat("yyMMdd").parse(dat);
                    }
                    long ms=System.currentTimeMillis()-theDate.getTime();
                    long age=(long)(ms/(1000.0*60*60*24*365.25));
                    if(age<0) age+=100;
                    System.out.println("Vek: "+age);
                    break;
                }
                else if(i==list.size()-1) System.out.println("Hledana osobna nenalezena.");
            }
        }
        catch (Exception e){
            System.out.println("Hledana osoba nenalezena");
        }
    }
}